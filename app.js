const fs = require("fs")
const Path = require('path')
const { Onfido } = require("@onfido/api");
const ObjectsToCsv = require('objects-to-csv');

const RESULT_FOLDER_NAME = 'KGB-archive'
const TIMEFRAME = 60000
const MAX_REQUESTS_PER_TIMEFRAME = 30
let  REQUEST_PER_TIMEFRAME_COUNTER = 0

const onfido = new Onfido({
    apiToken: 'ENTER_TOKEN_PLEASE'
    // Defaults to Region.EU (api.onfido.com), supports Region.US and Region.CA
    // region: Region.US
});

const wait = ms => {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}

const throttleWrapper = async promise => {
    REQUEST_PER_TIMEFRAME_COUNTER++
    if (REQUEST_PER_TIMEFRAME_COUNTER >= MAX_REQUESTS_PER_TIMEFRAME) {
        console.log('We are too fast for this API, need to wait for 1 minute, please standby')
        await wait(TIMEFRAME)
        REQUEST_PER_TIMEFRAME_COUNTER = 1
    }
    return promise
}

const dirCreateErrorHandler = err => {
    if (err) {
        console.log(err)
    } else {
        console.log("New directory successfully created.")
    }
}

const createDir = (path) => {
    try {
        if (fs.existsSync(path)) {
            console.log("Directory exists.")
            console.log(path)
        } else {
            fs.mkdir(path, dirCreateErrorHandler)
            console.log(path)
        }
    } catch(e) {
        console.log("An error occurred.")
    }
}

const convertToCsv = async (list, path) => {
    const csv = new ObjectsToCsv(list);
    await csv.toDisk(path);
}

const documentDownloader = async (file, dir, filetype) => {
    if (!file.id) {
        return
    }
    const filePath = Path.resolve(__dirname, dir, `${file.fileName}`)
    const writer = fs.createWriteStream(filePath)
    const apiRes = await throttleWrapper(onfido[filetype].download(file.id))
    console.log(apiRes.contentType)
    const readableStream = apiRes.asStream();
    readableStream.pipe(writer)
    return new Promise((res, rej) => {
        writer.on('finish', res)
        writer.on('error', rej)
    })
}

const app = async () => {
    try {
        const applicants = await throttleWrapper(onfido.applicant.list())
        console.log(applicants)
        createDir(RESULT_FOLDER_NAME)
        await convertToCsv(applicants, `${RESULT_FOLDER_NAME}/applicants.csv`)

        for (const applicant of applicants) {

            const applicantFolderName = `${RESULT_FOLDER_NAME}/${applicant.id}_${applicant.firstName}_${applicant.lastName}`
            createDir(`${applicantFolderName}`)

            const documents = await throttleWrapper(onfido.document.list(applicant.id))
            const documentsDir = `${applicantFolderName}/documents`
            createDir(`${documentsDir}`)
            await convertToCsv(documents, `${documentsDir}/documents.csv`)
            console.log(documents)
            for (const document of documents) {
                await documentDownloader(document, documentsDir, 'document')
            }

            const livePhotos = await throttleWrapper(onfido.livePhoto.list(applicant.id))
            const livePhotosDir = `${applicantFolderName}/livePhotos`
            createDir(`${livePhotosDir}`)
            await convertToCsv(livePhotos, `${livePhotosDir}/livePhotos.csv`)
            console.log(livePhotos)
            for (const livePhoto of livePhotos) {
                await documentDownloader(livePhoto, livePhotosDir, 'livePhoto')
            }

            const liveVideos = await throttleWrapper(onfido.liveVideo.list(applicant.id))
            const liveVideosDir = `${applicantFolderName}/liveVideos`
            createDir(`${liveVideosDir}`)
            await convertToCsv(liveVideos, `${liveVideosDir}/liveVideos.csv`)
            console.log(liveVideos)
            for (const liveVideo of liveVideos) {
                await documentDownloader(liveVideo, liveVideosDir, 'liveVideo')
            }

            const checks = await throttleWrapper(onfido.check.list(applicant.id))
            const checksDir = `${applicantFolderName}/checks`
            createDir(`${checksDir}`)
            await convertToCsv(checks, `${checksDir}/checks.csv`)
            console.log(checks)
            for (const check of checks) {
                const reports = await throttleWrapper(await onfido.report.list(check.id))
                const reportsDir = `${checksDir}/reports`
                createDir(`${reportsDir}`)
                await convertToCsv(reports, `${reportsDir}/reports.csv`)
                console.log(reportsDir)
            }
        }
    } catch (e) {
        console.log(e)
    }
}

app();